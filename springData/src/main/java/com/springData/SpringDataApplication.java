package com.springData;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataApplication.class, args);
	}

	@Bean
	CommandLineRunner databasePopulator(CarRepository repository) {
	    return args -> {
	        repository.save(new Car("e3", "bmw",30));
	        repository.save(new Car("e3", "bmw",15));
	        repository.save(new Car("e3", "bmw",22));
	    };
	}
	
}
