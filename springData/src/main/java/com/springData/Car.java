package com.springData;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Car {

	 @Id
	 @GeneratedValue
	 private Long id;
	 private String marka;
	 private String model;
	 private int yıl;

	 Car() {
	        // Bu argümansız constructor, JPA'in kayıttan nesne oluşturabilmesi için şart
	    }
	 
	 
		public Car(String marka, String model, int yıl) {
			
			this.marka = marka;
			this.model = model;
			this.yıl = yıl;
		}
		 
	
}
