package com.springData;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;



public interface CarRepository extends JpaRepository<Car, Long> {
	
	List<Car> findByYıl(int yıl);

	
}
