package com.springData;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cars")
public class CarController {
	
	private final CarRepository carRepository;
	
	public CarController(CarRepository carRepository) {
		
		this.carRepository=carRepository;
	}
	
	@GetMapping()
	List<Car> cars(){
	
		return carRepository.findAll();
	}
/*
	@GetMapping("/{id}")
	Car byId(@PathVariable Long id) {
		return carRepository.findOne(id);
	}
*/
	
	@PostMapping
	Car save(Car car) {
		
		return carRepository.save(car);
	}
	
	@DeleteMapping("/{car}")
	void delete(@PathVariable Car car) {
		carRepository.delete(car);
	}
	
	@GetMapping("/search")
    List<Car> searchByYıl(@RequestParam int yıl) {
        return carRepository.findByYıl(yıl);
    }
	
	
	
	
	
	
	
	
	
}
